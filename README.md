# Finquest Front-End Test

## Prerequisites

- You must use Bootstrap 5 and Angular latest version.
- The website must be responsive.
- Page must be organized for easy understanding by the user.
- All date displayed in format “YYYY/MM/DD”.
- You should create mock for API and it should be easy to unplug

Backend developer define API like this:

```API
• POST /api/people/List
    ◦ Input:
        ▪ Fisrtname: string
        ▪ Lastname: string
        ▪ AlreadyConnected: Boolean
    ◦ Ouput:
        ▪ Firstname: string
        ▪ Lastname: string
        ▪ LastConnectionDate: datetime could be null

• GET /api/people/{id}
    ◦ Output:
        ▪ Id: string
        ▪ Firstname: string
        ▪ Lastname: string
        ▪ Birthdate: Date [Nullable]
        ▪ Skills: Array of id (string) [Nullable]
        ▪ Department: id (string) [Nullable]
        ▪ ProfileCompletion: int (From 0 to 100)
        ▪ LastConnection: Datetime [Nullable]

• POST /api/people/update
    ◦ Input is object with
        ▪ Id: string
        ▪ Firstname: string
        ▪ Lastname: string
        ▪ Birthdate: Date
        ▪ Skills: Array of id (string) [Max 10 items]
        ▪ Department: id (string)
        ▪ Country: id (string)
    ◦ Output:
        ▪ Empty HTTP 200 if OK
        ▪ HTTP 400 if validation error which contains dictionary of error (Key = fieldname, Value=error message)
        ▪ Empty HTTP 500 if technical error (DB,…)

• GET api/reference/skills/{searchpattern}
    ◦ Searchpattern should filter result (DB contains more than 1500 skill in production)
    ◦ Output:
        ▪ List of object composed by
            • Id: string
            • Name: string

• GET api/reference/countries
    ◦ Output:
        ▪ List of object composed by
            • Id: string
            • Region: string (Africa, Europ, Asia, …)
            • Name: string

• GET api/reference/department
    ◦ Output:
        ▪ List of object composed by
            • Id: string
            • Name: string
```

## To do

- [x] API:
  - [x] POST /api/people/List
    - [x] Input:
      - [x] Fisrtname: string
      - [x] Lastname: string
      - [x] AlreadyConnected: Boolean
    - [x] Ouput:
      - [x] Firstname: string
      - [x] Lastname: string
      - [x] LastConnectionDate: datetime could be null
  - [x] GET /api/people/{id}
    - [x] Output:
      - [x] Id: string
      - [x] Firstname: string
      - [x] Lastname: string
      - [x] Birthdate: Date [Nullable]
      - [x] Skills: Array of id (string) [Nullable]
      - [x] Department: id (string) [Nullable]
      - [x] ProfileCompletion: int (From 0 to 100)
      - [x] LastConnection: Datetime [Nullable]
  - [x] POST /api/people/update
    - [x] Input is object with
      - [x] Id: string
      - [x] Firstname: string
      - [x] Lastname: string
      - [x] Birthdate: Date
      - [x] Skills: Array of id (string) [Max 10 items]
      - [x] Department: id (string)
      - [x] Country: id (string)
    - [x] Output:
      - [x] Empty HTTP 200 if OK
      - [x] HTTP 400 if validation error which contains dictionary of error (Key = fieldname, Value=error message)
      - [x] Empty HTTP 500 if technical error (DB,…)
  - [x] GET api/reference/skills/{searchpattern}
    - [x] Searchpattern should filter result (DB contains more than 1500 skill in production)
    - [x] Output:
      - [x] List of object composed by
        - Id: string
        - Name: string
  - [x] GET api/reference/countries
    - [x] Output:
      - [x] List of object composed by
        - Id: string
        - Region: string (Africa, Europ, Asia, …)
        - Name: string
  - [x] GET api/reference/department
    - [x] Output:
      - [x] List of object composed by
        - Id: string
        - Name: string
- [x] 1- Create a main application with:
  - [x] a. Main page application containing:
    - [x] Menu Sidebar
    - [x] Header bar with logo on the left side and connected user name on the right side
    - [x] Container for navigating on page
- [x] 2- Create a library project with:
  - [x] a. Page listing user:
    - Items are composed by:
      - Display name.
      - Has already connected.
      - Last connection date.
    - [x] User can navigate to detail page by clicking on row/panel.
    - [x] The list can be filtered by:
      - [x] First name.
      - [x] Last name.
      - [x] User already connected / never connected.
    - [x] The list can be ordered by:
      - [x] Last connection date.
      - [x] Last name (default order).
  - [x] b. Detail page for user:
    - By default:
      - [x] all fields on detail page must be read-only
      - [x] and the user can click on “Edit” button to switch few fields (specified by an editable flag on next list) editable.
    - The detail page is composed by:
      - [x] First name (Editable, Mandatory).
      - [x] Last name (Editable, Mandatory).
      - [x] Birthdate ~~(Editable)~~ (Editable, **_Mandatory_**).
      - [x] Skills ~~(Editable)~~ (Editable, **_Mandatory_**).
      - [x] Department (Editable, Mandatory).
      - [x] Country (Editable, Mandatory).
      - [x] Profile completion (percentage of user profile completion, calculated by server, must appear as a gauge or another graphical way).
      - [x] Last connection date (displayed “YYYY/MM/DD HH:mm”).

## Notes after work

- The Backend team didn't make the following APIs which important to FrontEnd team (me), so I made them:
  - POST `/api/people/List` must return Id field to use it for future requests.
  - GET `/api/department/:id` output a department found ID.
  - GET `/api/country/:id` output a department found ID.
  - GET `/api/skill?ids=[ids]` output skills found by the ID List.
- There's a conflicts between the Detail page @ `2-b` and API `POST /api/people/update` input, where:
  - The poblem address:
    - API input (required):
      - Birthdate: Date
      - Skills: Array of ID (string) [Max 10 items]
    - 2-b detail page (optional):
      - Birthdate (Editable)
      - Skills (Editable)
  - The available solutions:
    - Talk to the Backend team to update the API.
      - **Negative** empacts:
        - The BackEnd Team might not ready to refactor thier API.
        - The BackEnd Team might not have time to fix it fast.
        - There's no wait time to get the updates from the FrontEnd team.
        - Slow down the BackEnd team.
      - **Positive** empacts:
        - The FrontEnd cover the missing part to rout the UI/UX together.
        - Follow the End user needs.
        - The FrontEnd team continue with confidance.
    - Follow the API.
      - **Negative** empacts:
        - The UI/UX will not follow the end user needs.
        - The FrontEnd team have no communication/understanding to the BackEnd team.
        - The API is subject to miss routing the project.
      - **Positive** empacts:
        - The BackEnd team work faster.
        - The FrontEnd team will not suspanded.
  - The best solution:
    - Since there's no time to get the updates from Hervé (All candidates will contact him| He's busy with other things | The email might not reach him fast), So I will follow the API blindly, by make these fields mandatory too.

### Nice to have

Explain how I can put JWT security on HTTP call automatically.

#### The answer

We need to use the HttpClient to make authenticated HTTP requests. Since Angular supports HTTP [interceptors](https://angular.io/guide/http#intercepting-requests-and-responses), we can modify a request automatically before it is sent to the backend, We can use already made libraries (recommanded way) such as `@auth0/angular-jwt` by install it with the following command:

```bash
# installation with pnpm
pnpm i @auth0/angular-jwt

# installation with npm
npm install @auth0/angular-jwt

# installation with yarn
yarn add @auth0/angular-jwt
```

or by Create a new one which require maintanances and as a startup scaffold we can use already libraries code like [angularJwt](https://github.com/auth0/angular-jwt/blob/master/src/angularJwt).

The current steps demonstrate how to use a made already package `@auth/angular-jwt`:

1 - Install the angular-jwt library using PNPM: `pnpm i @auth0/angular-jwt --shamefull-hoist`.

2 - Import the `JwtModule` module and add it to your imports list. Call the forRoot method and provide a tokenGetter function. You must also add any domains to the allowedDomains, that you want to make requests to by specifying an allowedDomains array (Be sure to import the `HttpClientModule` as well).

```ts
import { JwtModule } from "@auth0/angular-jwt";
import { HttpClientModule } from "@angular/common/http";

export function tokenGetter() {
  return localStorage.getItem("access_token");
}

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    // ...
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["example.com"],
        disallowedRoutes: ["http://example.com/examplebadroute/"],
      },
    }),
  ],
})
export class AppModule {}
```

Any requests sent using Angular's HttpClient will automatically have a token attached as an Authorization header.

```ts
import { HttpClient } from "@angular/common/http";

export class AppComponent {
  constructor(public http: HttpClient) {}

  ping() {
    this.http.get("http://example.com/api/things").subscribe(
      (data) => console.log(data),
      (err) => console.log(err)
    );
  }
}
```

Much details with code examples can be found in the official repository
[Basic usage](https://github.com/auth0/angular2-jwt#usage-injection).
