/**
 * RE-usable function for both client & server sides.
 *
 * **Note** : the current validation function is VERY
 * simple and can be more advanced and more SECURE.
 */
export function Validator(input: any) {
  // The errors type definition
  type TError = {
    // Key = fieldname, Value=error message
    [key: string]: string;
  };
  // The errors which will contains errors of: Key = fieldname, Value=error message
  const _errors: TError = {};

  // The required fields
  const _Required = [
    'Id', // * FINE
    'Firstname', // * FINE
    'Lastname', // * FINE
    'Birthdate', // ! Conflict with UI & API
    'Skills', // ! Conflict with UI & API
    'Department', // * FINE
    'Country', // * FINE
  ];
  // The input fileds names
  const _inputFields = Object.keys(input);

  // loop through all required fileds and inject the error
  // object by the missing field by compare the
  // if the input fields doesnt include X required field
  // inject the Error object by the X required field
  for (let i = 0; i < _Required.length; i++) {
    // The X required field
    const _Rfield = _Required[i];

    if (!input[_Rfield]) {
      _errors[_Rfield] = `${_Rfield} should not be defined!`;
      continue;
    }

    // Check that Skills are a type of Array
    if (_Rfield === 'Skills') {
      // Skills length shoud be array and length greater than 0
      if (!Array.isArray(input[_Rfield]) || input.Skills?.length === 0) {
        _errors['Skills'] = `Minimum Skills is 1`;
        continue;
      }
      // Skills length shoud be equall or less than 10
      if (input.Skills?.length > 10) {
        _errors['Skills'] = 'You can set up to 10 maximum skills only';
        continue;
      }
    }
    if (!_inputFields.includes(_Rfield)) {
      _errors[_Rfield] = `${_Rfield} is required`;
      continue;
    }
    // trim the user field then ensure its not an empty field
    if (
      !Array.isArray(input[_Rfield]) &&
      input[_Rfield] &&
      input[_Rfield].trim().length === 0
    ) {
      _errors[_Rfield] = `${_Rfield} shouldn't be empty!`;
    }
  }

  return _errors;
}
