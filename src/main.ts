import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

// The following line import an Api simulate API structure made by
// the backend devs it will be mocked automaticly in developement env
import { ApiMock } from './apiMock/index';

if (environment.production) {
  enableProdMode();
} else {
  // Comment the following line to disable the Mocking Api service
  // and which will allow the requests to hit the real backend api.
  // If you want to remove/unplug the mocking service totally just
  // remove `apiMock` folder then uninstall `MirageJs`, `nanoid`
  // and `@faker-js/faker` from `devDependencies`.
  ApiMock({ prefix: 'api' });
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch((err) => console.error(err));
