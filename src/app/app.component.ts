import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  user = {
    fname: 'Abdelkarim',
    lname: 'Ben Hamida',
  };
  // title = 'finRequest';
  ShowNav = true;
  ToggleSideBar() {
    this.ShowNav = !this.ShowNav;
  }
}
