import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxEchartsModule } from 'ngx-echarts';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// Main project Components
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { E404Component } from './pages/e404/e404.component';

// Components from `lproject`
import { UserPageComponent } from 'projects/lproject/src/lib/user-page/user-page.component';
import { UserDetailComponent } from 'projects/lproject/src/lib/user-detail/user-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    E404Component,
    UserPageComponent,
    UserDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts'),
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
