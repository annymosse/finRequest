import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Main project Components
import { E404Component } from './pages/e404/e404.component';
import { HomeComponent } from './pages/home/home.component';
// import { UserDetailComponent } from './pages/user-detail/user-detail.component';

// Lib project components
import { UserPageComponent } from 'projects/lproject/src/lib/user-page/user-page.component';
import { UserDetailComponent } from 'projects/lproject/src/lib/user-detail/user-detail.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'peoples',
    children: [
      {
        path: '',
        component: UserPageComponent
      },
      {
        path: ':id/detail',
        component: UserDetailComponent,
      },
    ],
  },
  {
    path: '**',
    component: E404Component,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
