import { Model } from 'miragejs';
import { ModelDefinition } from 'miragejs/-types';
import { IUser } from 'projects/lproject/src/types/IUser';

export const UserModel: ModelDefinition<IUser> = Model.extend({});
