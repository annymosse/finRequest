import { Model } from 'miragejs';
import { ModelDefinition } from 'miragejs/-types';
import { ISkill } from 'projects/lproject/src/types/ISkill';

export const SkillModel: ModelDefinition<ISkill> = Model.extend({});
