import { Model } from 'miragejs';
import { ModelDefinition } from 'miragejs/-types';
import { IDepartment } from 'projects/lproject/src/types/IDepartment';

export const DepartmentModel: ModelDefinition<IDepartment> = Model.extend({});
