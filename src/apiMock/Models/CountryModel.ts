import { Model } from "miragejs";
import { ModelDefinition } from "miragejs/-types";
import { ICountry } from "projects/lproject/src/types/ICountry";

export const CountryModel: ModelDefinition<ICountry> = Model.extend({});
