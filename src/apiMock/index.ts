import {
  Server,
  Response,
  Registry,
  Factory,
  Model,
  Serializer,
} from 'miragejs';
import Schema from 'miragejs/orm/schema';
import { nanoid } from 'nanoid';
import { faker } from '@faker-js/faker';

// Helpers
import { Validator } from 'src/utils/validator';

// Models
import { UserModel, CountryModel, SkillModel, DepartmentModel } from './Models';
import { IUser } from 'projects/lproject/src/types/IUser';

type AppRegistry = Registry<
  {
    user: typeof UserModel;
    skill: typeof SkillModel;
    country: typeof CountryModel;
    department: typeof DepartmentModel;
  },
  {
    /* Factories definitions here */
  }
>;
type AppSchema = Schema<AppRegistry>;

export function ApiMock(arg: {
  prefix: string;
  subdomain?: string;
  ignoreURL?: string[];
}) {
  new Server({
    serializers: {
      application: Serializer,
    },
    models: {
      user: Model,
      skill: Model,
      country: Model,
      department: Model,
    },
    factories: {
      user: Factory.extend({
        Id: () => nanoid(),
        Firstname: () => faker.name.firstName(),
        Lastname: () => faker.name.lastName(),
        Birthdate() {
          return faker.helpers.randomize([
            faker.date
              .between('1975-01-01', '1999-01-01')
              .toISOString()
              .split('T')[0],
            null,
          ]);
        },
        ProfileCompletion() {
          return faker.datatype.number({ min: 0, max: 100 });
        },
        LastConnection() {
          return faker.helpers.randomize([
            faker.date.recent(faker.datatype.number({ max: 100 })),
            null,
          ]);
        },
        Skills: () => null,
        Department: () => null,
        Country: () => null,
      }),
      skill: Factory.extend({
        Id: () => nanoid(),
        Name: () => faker.name.jobTitle(),
      }),
      country: Factory.extend({
        Id: () => nanoid(),
        Name: () => faker.address.country(),
        Region() {
          const _regions = [
            'Asia',
            'Africa',
            'North America',
            'South America',
            'Antarctica',
            'Europe',
            'Australia',
          ];
          return faker.helpers.randomize(_regions);
        },
      }),
      department: Factory.extend({
        Id: () => nanoid(),
        Name: () => faker.commerce.department(),
      }),
    },

    seeds(server) {
      server.createList('user', 20);
      server.createList('skill', 500);
      server.createList('country', 100);
      server.createList('department', 250);
    },

    routes() {
      this.namespace = arg.prefix || '';
      this.urlPrefix = arg.subdomain || '';
      if (arg?.ignoreURL && arg?.ignoreURL.length > 0) {
        this.passthrough(arg.ignoreURL);
      }

      /**
       * * ==================================================
       * * The following routes made to help make the project
       * * getting work by mocking data which is important to
       * * the FrontEnd team, whenever the Backend API routes
       * * which defined in the test didn't include them.
       * * ==================================================
       */
      // Get skills by Array of id.
      this.get('skill', function (schema: AppSchema, request) {
        const res = schema
          .all('skill')
          .filter((skill) => request.queryParams['ids'].includes(skill.Id));
        return this.serialize(res).skills;
      });
      this.get('department/:id', function (schema: AppSchema, request) {
        const res = schema.findBy('department', { Id: request.params['id'] });
        return this.serialize(res).departments;
      });
      this.get('country/:id', function (schema: AppSchema, request) {
        const res = schema.findBy('country', { Id: request.params['id'] });
        return this.serialize(res).countries;
      });
      // * ==================================================

      /**
       * - Input:
       *  ▪ Fisrtname: string
       *  ▪ Lastname: string
       *  ▪ AlreadyConnected: Boolean
       * - Ouput:
       *  ▪ Firstname: string
       *  ▪ Lastname: string
       *  ▪ LastConnectionDate: datetime could be null
       */
      this.post('people/List', function (schema: AppSchema, request) {
        // The input type definition
        type TInput = {
          Firstname: string;
          Lastname: string;
          AlreadyConnected: boolean;
        };
        // Get the input data
        let input: TInput = JSON.parse(request.requestBody);

        // Holde the results temporarly
        let _res = schema.all('user');

        if (input.Firstname?.trim().length > 0) {
          _res = _res.filter((usr) => {
            return usr.Firstname.toLowerCase().includes(
              input.Firstname.toLowerCase()
            );
          });
        }
        if (input.Lastname?.trim().length > 0) {
          _res = _res.filter((usr) => {
            return usr.Lastname.toLowerCase().includes(
              input.Lastname.toLowerCase()
            );
          });
        }

        if (input.AlreadyConnected) {
          _res = _res.filter((usr) => usr.LastConnection !== null);
        } else {
          _res = _res.filter((usr) => usr.LastConnection === null);
        }

        let res: { users: IUser[] } = this.serialize(_res);

        return res.users.map((u) => {
          return {
            Id: u.Id, // ! THIS FILED IS REQUIRED TO GET USER BY ID
            Firstname: u.Firstname,
            Lastname: u.Lastname,
            LastConnectionDate: u.LastConnection,
          };
        });
      });

      /**
       * - Output:
       *  ▪ Id: string
       *  ▪ Firstname: string
       *  ▪ Lastname: string
       *  ▪ Birthdate: Date [Nullable]
       *  ▪ Skills: Array of id (string) [Nullable]
       *  ▪ Department: id (string) [Nullable]
       *  ▪ ProfileCompletion: int (From 0 to 100)
       *  ▪ LastConnection: Datetime [Nullable]
       */
      this.get('people/:id', (schema: AppSchema, request) => {
        // Hold the result in a temp variable
        const _res = schema.findBy('user', { Id: request.params['id'] });

        // Check if there's a user or return empty object
        if (!_res) return {};

        // The output data shape
        return {
          Id: _res.Id,
          Firstname: _res.Firstname,
          Lastname: _res.Lastname,
          Birthdate: _res.Birthdate,
          Skills: _res.Skills,
          Department: _res.Department,
          ProfileCompletion: _res.ProfileCompletion,
          LastConnection: _res.LastConnection,
          // TODO BackEnd team should include this field
          Country: _res.Country, // ! This field is required to get the Country name.
        };
      });

      /**
       * - Input is object with
       *  ▪ Id: string
       *  ▪ Firstname: string
       *  ▪ Lastname: string
       *  ▪ Birthdate: Date
       *  ▪ Skills: Array of id (string) [Max 10 items]
       *  ▪ Department: id (string)
       *  ▪ Country: id (string)
       * - Output:
       *   ▪ Empty HTTP 200 if OK
       *   ▪ HTTP 400 if validation error which contains dictionary of error (Key = fieldname, Value=error message)
       *   ▪ Empty HTTP 500 if technical error (DB,…)
       */
      this.post('people/update', (schema: AppSchema, request) => {
        // The input tape definition
        type TInput = {
          Id: string;
          Firstname: string;
          Lastname: string;
          Birthdate: string;
          Skills: string[]; // id (Max 10 items)
          Department: string; // id
          Country: string; // id
          [key: string]: any;
        };
        // The user inputs
        const input: TInput = JSON.parse(request.requestBody);

        const _errors = Validator(input);

        // HTTP 400 if validation error which contains dictionary of error (Key = fieldname, Value=error message)
        if (Object.keys(_errors).length > 0) {
          return new Response(
            400,
            { 'Content-Type': 'application/json' },
            { errors: _errors }
          );
        }

        try {
          const _user = schema.findBy('user', { Id: input.Id });
          if (!_user) throw new Error('DB,...Errors');
          const _changes = {
            Firstname: input.Firstname,
            Lastname: input.Lastname,
            Birthdate: input.Birthdate,
            Country: input.Country || _user.Country,
            Department: input.Department,
            Skills: input.Skills,
          };
          _user.update(_changes);

          // Empty HTTP 200 if OK
          return new Response(200);
        } catch (error) {
          // Empty HTTP 500 if technical error (DB,…)
          return new Response(500);
        }
      });

      /**
       * `Searchpattern` should filter results
       * - Output:
       *   ▪ List of object composed by
       *       • Id: string
       *       • Name: string
       */
      this.get(
        'reference/skills/:searchpattern',
        function (schema: AppSchema, request) {
          // Get the search pattern param
          const _search = request.params['searchpattern'];

          const res = schema
            /* Get all Skills */
            .all('skill')
            /**
             * Filter the skills by set skill name & searchparam to lower case
             * and check if the database skill name include the searchpattern
             */
            .filter((skill) => {
              return skill.Name.toLowerCase().includes(_search.toLowerCase());
            });

          return this.serialize(res).skills;
        }
      );

      /**
       * Retrun all countries
       * - Output:
       *  ▪ List of object composed by
       *     • Id: string
       *     • Region: string (Africa, Europ, Asia, …)
       *     • Name: string
       */
      this.get('reference/countries', function (schema: AppSchema) {
        const res = schema.all('country');
        return this.serialize(res).countries;
      });

      /**
       * Retrun all departments
       * - Output:
       *  ▪ List of object composed by
       *     • Id: string
       *     • Name: string
       */
      this.get('reference/department', function (schema: AppSchema) {
        const res = schema.all('department');
        return this.serialize(res).departments;
      });
    },
  });
}
