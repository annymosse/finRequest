import { Component,OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface IOutputUser {
  Id: string;
  Firstname: string;
  Lastname: string;
  LastConnectionDate: string;
}


@Component({
  selector: 'lib-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {
  constructor(private http: HttpClient) {}
  users: IOutputUser[] = [];

  filters = {
    Firstname: '',
    Lastname: '',
    AlreadyConnected: false,
  };
  FilterUsers() {
    this.http
      .post<IOutputUser[]>('/api/people/List', this.filters)
      .subscribe((data) => {
        this.users = data;
      });
  }

  sort = {
    by: 'LN', // By default Order by Last name
    ascend: true,
  };
  orderByLN() {
    if (this.sort.ascend) {
      this.users.sort((a, b) => b.Lastname.localeCompare(a.Lastname));
    } else {
      this.users.sort((a, b) => a.Lastname.localeCompare(b.Lastname));
    }
    this.sort = {
      by: 'LN',
      ascend: !this.sort.ascend,
    };
  }
  orderByLC() {
    if (this.sort.ascend) {
      this.users.sort(
        (a, b) =>
          new Date(b.LastConnectionDate).getTime() -
          new Date(a.LastConnectionDate).getTime()
      );
    } else {
      this.users.sort(
        (a, b) =>
          new Date(a.LastConnectionDate).getTime() -
          new Date(b.LastConnectionDate).getTime()
      );
    }
    this.sort = {
      by: 'LC',
      ascend: !this.sort.ascend,
    };
  }

  ngOnInit(): void {};
}
