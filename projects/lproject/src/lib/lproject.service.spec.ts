import { TestBed } from '@angular/core/testing';

import { LprojectService } from './lproject.service';

describe('LprojectService', () => {
  let service: LprojectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LprojectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
