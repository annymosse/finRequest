import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LprojectComponent } from './lproject.component';

describe('LprojectComponent', () => {
  let component: LprojectComponent;
  let fixture: ComponentFixture<LprojectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LprojectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LprojectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
