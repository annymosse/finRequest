import { DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

// Lib project Components
import { LprojectComponent } from './lproject.component';

@NgModule({
  declarations: [LprojectComponent],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([]),
  ],
  providers: [DatePipe],
  exports: [LprojectComponent],
})
export class LprojectModule {}
