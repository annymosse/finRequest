import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ICountry } from 'projects/lproject/src/types/ICountry';
import { IDepartment } from 'projects/lproject/src/types/IDepartment';
import { ISkill } from 'projects/lproject/src/types/ISkill';
import { IUser } from 'projects/lproject/src/types/IUser';

import { Validator } from '../../utils/validator';

@Component({
  selector: 'lib-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
})
export class UserDetailComponent implements OnInit {
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  // Used for cache request data as source of truth.
  _user: IUser | { [key: string]: any } = {};
  user: IUser | { [key: string]: any } = {};
  // Switch between mode View and mode Edit.
  editUser = false;
  // The errors of user fields.
  errors: { [key: string]: any } = {};
  // The user profile progress graph options.
  options: any;
  // The user progress
  progress: number = 0;

  // Used for cache request data as source of truth.
  _countries: ICountry[] | [] = [];
  // Used for the filtered data.
  countries: ICountry[] | [] = [];
  userCountry: { txt: string; data: ICountry | any } = {
    // Used for the inpult ngModel.
    txt: '',
    // Used for final user data.
    data: {},
  };
  /**
   * Trigger this function only once at initial
   * startup the results stored at memory side
   */
  getCountries() {
    this.http.get<ICountry[]>('/api/reference/countries').subscribe((data) => {
      this._countries = data;
      this.countries = data;
    });
  }
  FilterCountries() {
    const _txt = this.user.Country;
    if (_txt && _txt.trim().length > 0) {
      this.countries = this._countries.filter((c) =>
        c.Name.toLowerCase().includes(_txt.trim().toLowerCase())
      );
    } else {
      this.countries = [...this._countries];
    }
  }
  SetFirstCountry() {
    this.userCountry.data = this.countries[0];
    this.userCountry.txt = this.countries[0].Name;
  }

  // Used for cache request data as source of truth.
  _departments: IDepartment[] | [] = [];
  // Used for the filtered data.
  departments: IDepartment[] | [] = [];
  userDepartment: { txt: string; data: IDepartment | any } = {
    // Used for the inpult ngModel.
    txt: '',
    // Used for final user data.
    data: {},
  };
  /**
   * Trigger this function only once at initial
   * startup the results stored at memory side
   */
  getDepartments() {
    this.http
      .get<IDepartment[]>('/api/reference/department')
      .subscribe((data) => {
        this._departments = data;
        this.departments = data;
      });
  }
  FilterDepartments() {
    const _txt = this.userDepartment.txt;
    if (_txt && _txt.trim().length > 0) {
      this.departments = this._departments.filter((c) =>
        c.Name.toLowerCase().includes(_txt.trim().toLowerCase())
      );
    } else {
      this.departments = [...this._departments];
    }
  }
  SetFirstDepartment() {
    this.userDepartment.data = this.departments[0];
    this.userDepartment.txt = this.departments[0].Name;
  }

  // TODO: Improve the filted Skills to show only unique Skill's name.
  // Used for cache request data as source of truth.
  _skills: ISkill[] | [] = [];
  // Used for real data which keep change by the user search.
  skills: ISkill[] | [] = [];
  userSkills: { txt: string; data: ISkill[]; skillNames: string[] } = {
    // Used for the inpult ngModel.
    txt: '',
    // Used for final user data.
    data: [],
    // Used to cache the user skill names for filter perpose.
    skillNames: [],
  };
  /**
   * This function run each time the user change the input
   * value to get a real data from the server database which
   * match the input's search pattern.
   */
  getSkills() {
    const _txt = this.userSkills.txt;
    if (!_txt || (_txt && _txt.trim().length === 0)) return;

    // Reduce the request cost by use cached skills for
    // text input with length less than 4 chars of length.
    if (_txt.trim().length < 4) {
      this.skills = this._skills;
      return;
    }

    // Get the real skills data from the server side.
    this.http
      .get<ISkill[]>('/api/reference/skills/' + _txt.replace(/\//g, ''))
      .subscribe((data) => {
        // Update the cache by only get the skills which
        // the user didn't select already.
        this._skills = data;

        // Set a clone of the new cached data.
        this.skills = [...this._skills];
      });
  }
  SelectSkill(skill?: ISkill) {
    // User slected a new skill
    if (skill && this.userSkills.data.length < 10) {
      // Add the new skill to the user data.
      this.userSkills.data.push(skill);
      // Add the skill name to reduce the filter.
      this.userSkills.skillNames.push(skill.Name);

      // Update the cache by remove the already selected skills.
      this._skills = this._skills.filter((s) => {
        return !this.userSkills.skillNames.includes(s.Name.toLocaleLowerCase());
      });
    } else {
      // Blur Event code here ...
    }
    // Reset the skill input field for a new search.
    this.userSkills.txt = '';
  }
  RemoveSkill(skill: ISkill) {
    for (let i = 0; i < this.userSkills.data.length; i++) {
      const s = this.userSkills.data[i];
      if (s.Id === skill.Id) {
        this.userSkills.data.splice(i, 1);
        break;
      }
    }
  }

  /**
   * Edit the user info.
   */
  EditUser() {
    // Clone the current user data to recover them at cancelation.
    this._user = { ...this.user };
    // Switch the view mode to edit mode.
    this.editUser = true;
  }
  /**
   * Cancel the Edit mode and rollback the user data.
   *
   * __TODO__ need to tweak it more :p
   */
  CancelEdit() {
    // Switch to the view mode
    this.editUser = false;
    // turn back the user data from the cloned one.
    this.user = { ...this._user };
  }
  /**
   * Validate the user data by check the required fields
   * all required fields aren't null or empty then POST
   * the new user data to save it at server side database.
   */
  SaveUser() {
    if (!this.user) return;

    // Set the user data to the real ones to validate them next.
    this.user.Skills = this.userSkills.data.map((s) => s.Id);
    this.user.Department = this.userDepartment.data.Id;
    this.user.Country = this.userCountry.data.Id;

    /**
     * Validate the user input's fileds at client side
     * to reduice the requests cost by RE-using same
     * validation function at server side.
     */
    const _errs = Validator(this.user);

    // If there's any error set it to the errors object.
    if (Object.keys(_errs).length > 0) {
      this.errors = _errs;
      return;
    }
    // Post the data to the server.
    this.http.post('/api/people/update', this.user).subscribe(
      () => {
        // Reset the errors
        this.errors = {};
        // Switch to the view mode
        this.editUser = false;
      },
      (err) => {
        // Assign the errors from remote to local errors.
        this.errors = err.error.errors;
        console.error(err);
      }
    );
  }

  ngOnInit(): void {
    const _id = this.route.snapshot.paramMap.get('id')!;

    this.http.get<IUser>('/api/people/' + _id).subscribe((data) => {
      if (Object.keys(data).length === 0) {
        this.router.navigate(['/peoples']);
        return;
      }

      // Check if the user have skills and get these skills
      if (data.Skills) {
        this.http
          .get<ISkill[]>('/api/skill?ids=' + JSON.stringify(data.Skills))
          .subscribe((res) => {
            this.userSkills.data = res;
            this.userSkills.skillNames = res.map((s) => s.Name);
          });
      }

      // Get the user Department data if the departmentId is defined.
      if (data.Department) {
        this.http
          .get<IDepartment>('/api/department/' + data.Department)
          .subscribe((data) => {
            this.userDepartment.data = data;
            this.userDepartment.txt = data.Name;
          });
      }

      // Get the user Country data if the CountryId is defined.
      if (data.Country) {
        this.http
          .get<ICountry>('/api/country/' + data.Country)
          .subscribe((data) => {
            this.userCountry.data = data;
            this.userCountry.txt = data.Name;
          });
      }

      this.getCountries();
      this.getDepartments();
      // * Initail skill list
      this.http.get<ISkill[]>('/api/reference/skills/a').subscribe((data) => {
        this._skills = data;
        this.skills = [...this._skills];
      });
      this._user = data;
      this.user = data;
      this.progress = data.ProfileCompletion;
      this.options = {
        series: [
          {
            name: 'Profile Completion',
            type: 'pie',
            radius: ['40%', '70%'],
            avoidLabelOverlap: false,
            itemStyle: {
              borderRadius: this.progress >= 99 ? 0 : 10,
              borderColor: '#fff',
              borderWidth: 2,
            },
            cursor: 'default',
            data: [
              {
                value: this.progress,
                name: this.progress + '%',
                itemStyle: { color: '#38b000' },
                emphasis: { disabled: true },
                label: {
                  show: true,
                  position: 'center',
                  fontSize: '2rem',
                  fontWeight: 'bold',
                  color: this.progress >= 50 ? '#38b000' : '#2E6EFD',
                  cursor: 'default',
                },
              },
              {
                value: 100 - this.progress,
                name: 'Progress',
                itemStyle: { color: '#2E6EFD' },
                emphasis: { disabled: true },
                label: {
                  show: false,
                },
              },
            ],
          },
        ],
      };
    });
  }
}
