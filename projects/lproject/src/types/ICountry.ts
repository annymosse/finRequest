export interface ICountry {
  Id: string;
  Name: string;
  Region: string;
}
