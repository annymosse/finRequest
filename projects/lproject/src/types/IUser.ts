import { Datetime } from "./TDivers";

export interface IUser {
  id?: string;
  Id: string;
  Firstname: string;
  Lastname: string;
  Birthdate: Datetime | null;
  Skills: string[] | null; //id;
  Department: string | null; //id;
  ProfileCompletion: number; //int (From 0 to 100);
  LastConnection: Datetime | null;
  Country: string; //id
}
